package az.ingress.lesson_3.model;

import az.ingress.lesson_3.dto.BookRequest;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String title;
    String author;
    Integer pageCount;

    public Book(BookRequest dto) {
        this.title = dto.getTitle();
        this.author = dto.getAuthor();
        this.pageCount = dto.getPageCount();
    }
}
