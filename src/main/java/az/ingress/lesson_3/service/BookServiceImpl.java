package az.ingress.lesson_3.service;

import az.ingress.lesson_3.dto.BookRequest;
import az.ingress.lesson_3.dto.BookResponse;
import az.ingress.lesson_3.model.Book;
import az.ingress.lesson_3.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;

    @Override
    public int createBook(BookRequest request) {
        Book book = Book.builder()
                .author(request.getAuthor())
                .title(request.getTitle())
                .pageCount(request.getPageCount())
                .build();
        bookRepository.save(book);
        return book.getId();
    }

    @Override
    public BookResponse updateBook(Integer id, BookRequest request) {
        Book found = bookRepository.findById(id).orElseThrow(() -> new RuntimeException("book not found"));
        found.setTitle(request.getTitle());
        found.setAuthor(request.getAuthor());
        found.setPageCount(request.getPageCount());
        return new BookResponse(found);
    }

    @Override
    public BookResponse getBookById(Integer id) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new RuntimeException("Book not found"));
        return new BookResponse(book);

    }

    @Override
    public List<BookResponse> getAllBooks() {
        return bookRepository.findAll().stream().map(BookResponse::new).toList();
    }

    @Override
    public void deleteBookById(Integer id) {
        bookRepository.deleteById(id);
    }
}
