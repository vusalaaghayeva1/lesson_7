package az.ingress.lesson_3;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@Slf4j
@RequiredArgsConstructor
public class Lesson3Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Lesson3Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
