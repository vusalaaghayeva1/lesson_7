FROM alpine:3.15
RUN apk add --no-cache openjdk17
WORKDIR /app
COPY build/libs/lesson_3-1.0.jar /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/lesson_3-1.0.jar"]